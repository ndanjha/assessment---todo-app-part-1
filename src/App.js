import React, { Component } from "react";
import "./index.css";
import todosList from "./todos.json";

class App extends Component {
  state = {
    todos: todosList,
    value: ""
  };

  handleChange = event => {
    this.setState({ value: event.target.value}); 
  }

  handleCreateTodo = event => {
    if (event.key === "Enter"){
    const newTodo ={
      userId: 1,
      id: Math.floor(Math.random() * 10000),
      title: this.state.value,
      completed: false
    };
    const newTodos = this.state.todos.slice();
    newTodos.push(newTodo);
    this.setState({todos: newTodos, value: ''})
  }
  };
  handleDeleteTodo = todoIdToDelete => (event) => {
    //create a copy
    const newTodos = this.state.todos.slice();
// modify copy
    const todoIndexToDelete = newTodos.findIndex(todo => {
      if (todo.id === todoIdToDelete){
        return true;
      } else {
        return false;
      }
    })
    newTodos.splice(todoIndexToDelete, 1 )
  //overwrite original copy
    this.setState({todos: newTodos})
  }

  handleDeleteSelected = () => {
    const newTodos = this.state.todos.filter(todo => {
      if(todo.completed === true){
        return false
      } else{
        return true
      }
    })
    this.setState({todos: newTodos});
  }

  completedToggle = (todoIdToToggle) => (event) =>{
   const newTodo = this.state.todos.map(todo => {
     if(todo.id === todoIdToToggle){
     if(todo.completed === true){
       return todo.completed = false;
     } else {
       todo.completed = true;
     } }return todo
   });
   this.setState({todos: newTodo});
  }

  completedLineThrough = (todos) => {
    if(todos.completed === true){
      return todos.completed = true;
    }else{
      return todos.completed = false;
    }
  };


  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            autofocus
            value={this.state.value}
            onChange={this.handleChange}
            onKeyDown={this.handleCreateTodo}
          />
        </header>
        <TodoList handleDeleteTodo={this.handleDeleteTodo} todos=
        {this.state.todos} completedToggle={this.completedToggle}/>
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button className="clear-completed" onClick={this.handleDeleteSelected}>Clear completed</button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completedLineThrough ? "completed" : ""}>
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            checked={this.props.completedLineThrough}
            onChange={this.props.completedToggle}
          />
          <label>{this.props.title}</label>
          <button className="destroy" onClick=
          {this.props.handleDeleteTodo} />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map(todo => (
            <TodoItem key={todo.id} handleDeleteTodo= {this.props.handleDeleteTodo(todo.id)
            }
             title={todo.title} 
             completedLineThrough={todo.completed}
             completedToggle={this.props.completedToggle(todo.id)}/>
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
